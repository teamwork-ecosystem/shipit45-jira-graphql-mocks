const { ApolloServer, gql } = require('apollo-server');
const { mergeTypeDefs } = require('./schemaTools');
const { generateQueryObject, generateTypeResolvers } = require('./dataGen');
const { parse } = require('graphql');

const typeDefs = mergeTypeDefs();
const parsedDefs = parse(typeDefs);

const resolvers = {
    Query: generateQueryObject(parsedDefs),
    ...generateTypeResolvers(parsedDefs),
}
const server = new ApolloServer({ typeDefs, resolvers });

server.listen(process.env.PORT || 4000).then(({ url }) => {
  console.log(`🚀  Server ready at ${url}`);
});
