const R = require('ramda');
const faker = require('faker');

const dummyResolver = () => null;

const genMultiple = (gen, count = 10) => () => R.times(gen, count);

const fieldByNameResolvers = {
    'description': faker.lorem.sentences,
    'tags': genMultiple(faker.random.word),
    'key': () => faker.random.alphaNumeric(10),
};
const randomAri = () => faker.random.alphaNumeric(20);

const fieldByTypeResolvers = {
    ID: randomAri,
    Int: () => faker.random.number(),
    Double: () => faker.random.float(),
    Long: () => faker.random.number(),
    String: () => faker.lorem.words(),
    Boolean: () => faker.random.boolean(),
};

const typeResolvers = {};

const findTypeKind = (fd, kind) => {
    return fd.kind === kind ? fd : fd.type ? findTypeKind(fd.type, kind) : null
};

const getType = fd => {
    return {
        name: findTypeKind(fd.type, 'NamedType').name.value,
        isArray: findTypeKind(fd.type, 'ListType') ? true : false,
        // todo: this does not handle the inner "!" in [!]!. Not a biggie, though.
        isNullable: fd.type.kind === 'NonNullType',
    }
};

const getFieldGenerator = f => {
    // todo: handle nullable types by randomly creating null fields
    const fieldType = getType(f);
    // Precedence:
    //   1. resolve by field name
    //   2. resolve by object type
    //   3. resolve by scalar type
    const coreGen =
        R.find(r => {console.log(fieldType.name, r); return fieldType.name.endsWith(r)}, R.keys(fieldByNameResolvers))
        || R.prop(fieldType.name, typeResolvers)
        || R.prop(fieldType.name, fieldByTypeResolvers)
        || dummyResolver;

    return fieldType.isArray ? genMultiple(coreGen) : coreGen;
}

const genericTypeResolver = (d, parsedSchema, isExtension = false) => () => {
    let extraFields = {}; 
    const g = R.fromPairs(
        R.map(f => [f.name.value, getFieldGenerator(f)], d.fields)
    );

    if (!isExtension) {
        const extensions = R.reduce(
            (acc, curr) => R.merge(acc, genericTypeResolver(curr, parsedSchema, true)()),
            {},
            R.filter(e => (e.kind === 'ObjectTypeExtension') && (e.name.value === d.name.value), parsedSchema.definitions)
        );

        console.log('extensions', extensions);
        extraFields = { __typeName: d.name.value, ...extensions };
    }
    
    return R.merge(extraFields, g);
};

const getOrAddResolver = (d, parsedSchema, gen) => {
    const key = d.name.value;
    if (!typeResolvers[key]) {
        typeResolvers[key] = gen(d, parsedSchema);
    }

    return typeResolvers[key];
}

const genericUnionResolver = d => () => {
    const possibleTypes = R.map(R.path(['name', 'value']), d.types);
    const chosenType = faker.random.arrayElement(possibleTypes);
    return typeResolvers[chosenType]();
};

const genericInterfaceResolver = (d, parsedSchema) => () => {
    const possibleTypes = R.map(R.path(['name', 'value']), R.filter(def => R.any(R.pathEq(['name', 'value'], d.name.value), def.interfaces || []), parsedSchema.definitions));
    const chosenType = faker.random.arrayElement(possibleTypes);
    return typeResolvers[chosenType]();
}

const definitionResolvers = {
    'ObjectTypeDefinition': (d, parsedSchema) => getOrAddResolver(d, parsedSchema, genericTypeResolver),
    'UnionTypeDefinition': (d, parsedSchema) => getOrAddResolver(d, parsedSchema, genericUnionResolver),
    'InterfaceTypeDefinition': (d, parsedSchema) => getOrAddResolver(d, parsedSchema, genericInterfaceResolver),
}

const getGenerators = (parsedSchema) => {
    const g = R.map(
        d => [d.name.value, R.propOr(dummyResolver, d.kind, definitionResolvers)(d, parsedSchema)],
        R.filter(d => d.kind !== 'ObjectTypeExtension', parsedSchema.definitions));
    return R.fromPairs(g);
}

// todo: idea -- any way to make this aware of fragments, so it
// resolves to one of the types listed under it? As in:
// node(id: "123") { ...on Issue { description } }
const generateQueryObject = (parsedSchema) => {
    // todo: remove this side effect -- the getGenerators method
    // populates the type generators in a global const (yup).
    getGenerators(parsedSchema);
    const defs = R.filter(
        R.pathEq(['name', 'value'], 'Query'), parsedSchema.definitions);

    const fields = R.fromPairs(R.map(
        fd =>  [fd.name.value, getFieldGenerator(fd)],
        R.flatten(R.map(R.prop('fields'), defs))
    ));

    return fields;
}

const generateTypeResolvers = (parsedSchema) => {
    const abstractKinds = [
        'InterfaceTypeDefinition',
        'UnionTypeDefinition'
    ];

    const res = 
        R.reduce(
            (accum, d) => R.merge(accum, { [d.name.value] : { __resolveType: (data) => data.__typeName } }),
            {},
            R.filter(d => R.contains(d.kind, abstractKinds), parsedSchema.definitions)
        );

    return res;
}

module.exports = { getGenerators, generateQueryObject, generateTypeResolvers };