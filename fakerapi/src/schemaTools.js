const R = require('ramda');
const { parse } = require('graphql');
const fs = require('fs');
const shell = require('shelljs');
const XRegExp = require('xregexp');

const mergeTypeDefs = () => {
    fs.readdirSync('./src/schemas')
    const rex = R.curry((pattern, repl, input) => XRegExp.replace(input, pattern, repl));
    const rg = [
        // fixme: get named group right
        rex(/^service.*\{([.\s\S]*)\}/, '$1'),
        rex(/<-[\t \$\.\w]*/, ``),
    ]

    const input = R.map(
        f => fs.readFileSync(f, 'utf8') + '\n',
        shell.ls('./src/schemas/*.gql')
    );

    const m = R.map(
        x => R.reduce((acc, curr) => curr(acc), x, rg),
        input
    );

    const mergedDefs = R.reduce(R.concat, '', m);

    const rxg = /\w+Connection\b/g
    const l = 'Connection'.length
    const extraDefs =
        R.reduce(
            R.concat,
            '',
            R.map(
                c => {
                    const n = c.slice(0, -l)
                    return `
    # An edge in a ${n} connection.
    type ${n}Edge {
        # The node at the the edge.
        node: ${n}
        # The cursor to this edge
        cursor: String!
    }

    type ${c} {
        pageInfo: PageInfo
        edges: [${n}Edge]
        nodes: [${n}]
    }
    `
                },
                R.uniq(R.match(rxg, mergedDefs))
            )
        );

    return mergedDefs + extraDefs;
};

const parseSchemas = () => {
    const schema = mergeTypeDefs();
    return parse(schema);
}

module.exports = {
    mergeTypeDefs,
    parseSchemas,
};